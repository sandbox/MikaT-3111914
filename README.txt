-- SUMMARY --

Google Stackdriver Drupal module.

-- INSTALLATION --

    1. Enable the module.
    2. Place required variables to your settings.php